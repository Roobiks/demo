

function User(socket) {
	this.socket = socket;
	// assign a random number to User.
	this.id = "1" + Math.floor( Math.random() * 1000000000);
	
}

function Room() {
	this.users = [];
}

Room.prototype.addUser = function(user){
	this.handleOnUserMessage(user);
	this.users.push(user);
	
	var room = this;
	
	// handle user closing
	user.socket.onclose = function(){
		console.log("A connection left");
		room.removeUser(user);
	}
};

// remove player
Room.prototype.removeUser = function(user) {
	// loop to find user
	for (var i=this.users.length; i>=0; i--){
		if (this.users[i] === user){
			this.users.splice(i,1);
		}
	}
};

// send message to all
Room.prototype.sendAll = function(message,user){
		for (var i=0, len=this.users.length; i<len; i++){
			if (this.users[i] === user){}
			var message = "(Add)";
			this.users[i].socket.send(message);
	}
};

// check incoming messages
Room.prototype.handleOnUserMessage = function(user) {
	var room = this;
	user.socket.on("message", function(message) {
		
		if (message=="(Add)"){
			message = "(Add)";
			room.sendAll(message,user);}
		else{}
		});
	
};

module.exports.User = User;
module.exports.Room = Room;